class freebsd::base {
  # Packages
  package {
    [
      'ssmtp',
      'vim',
    ]:
      ensure => installed,
  }

  # Make periodic logs go to files instead of mail.
  file { '/etc/periodic.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/periodic.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # Redirect syslog to Lysator's server.
  file { '/etc/syslog.d/lysator.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/syslog.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # set puppet server
  file { '/usr/local/etc/puppet/puppet.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/puppet.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # restart puppet agent when needed (after being updated)
  ensure_packages(['bash', 'jq'])
  file { '/usr/local/sbin/restart_puppet_if_needed.sh':
    ensure => file,
    source => 'puppet:///modules/freebsd/restart_puppet_if_needed.sh',
    owner => root,
    group => wheel,
    mode => '0755',
  }
  cron { 'restart puppet if needed':
    command => '/usr/local/sbin/restart_puppet_if_needed.sh',
    minute => [7, 27],
  }

  # Configure ssmtp
  file { '/usr/local/etc/ssmtp/ssmtp.conf':
    ensure  => file,
    content => template('freebsd/ssmtp.conf.erb'),
    owner   => 'root',
    group   => 'ssmtp',
    mode    => '0640',
  }

  file { '/etc/mail/mailer.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/mailer.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # Disable sendmail.
  file { '/etc/rc.conf.d/sendmail':
    ensure => file,
    source => 'puppet:///modules/freebsd/rc.conf.d/sendmail',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # Lysator's default DNS configuration.
  file { '/etc/resolv.conf':
    path   => '/etc/resolv.conf',
    source => 'puppet:///modules/freebsd/resolv.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }
}
