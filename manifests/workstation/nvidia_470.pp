#
class freebsd::workstation::nvidia_470 {
  file_line { 'Enable linux support':
    path => '/boot/loader.conf',
    line => 'linux_load="YES"',
  }
  ~> exec { '/sbin/kldload linux':
    refreshonly => true,
  }
  -> package {
    [
      'nvidia-driver-470',
    ]:
      ensure   => installed,
  }
  -> file_line { 'Enable nvidia driver':
    path => '/boot/loader.conf',
    line => 'nvidia-modeset_load="YES"',
  }
  ~> exec { '/sbin/kldload nvidia-modeset':
    refreshonly => true,
  }

  file_line { 'Fix broken vt-terminals':
    path => '/boot/loader.conf',
    line => 'hw.vga.textmode=1',
  }
}
