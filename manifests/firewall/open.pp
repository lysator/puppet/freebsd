#
class freebsd::firewall::open {
  file { '/etc/rc.conf.d/ipfw':
    ensure => file,
    source => 'puppet:///modules/freebsd/rc.conf.d/firewall_open',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }
}
