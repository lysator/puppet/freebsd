class freebsd::ldap::root {
  file_line { 'Allow only root login':
    path   => '/etc/master.passwd',
    line   => '+@root::::::::/var/empty:/bin/tcsh',
    notify => Exec['Update /etc/passwd'],
  }
  file_line { 'Disallow normal user login':
    path   => '/etc/master.passwd',
    line   => '+:::::::::/sbin/nologin',
    notify => Exec['Update /etc/passwd'],
  }
  exec { 'Update /etc/passwd':
    command     => '/usr/sbin/pwd_mkdb -p /etc/master.passwd',
    refreshonly => true,
  }
}
