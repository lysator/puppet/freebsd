#
class freebsd::vindbrygga {
  file { '/usr/local/etc/ssh_vindbrygga':
    ensure => directory,
  }

  file { '/usr/local/etc/ssh_vindbrygga/sshd_config':
    ensure => file,
    source => 'puppet:///modules/freebsd/vindbrygga/sshd_config',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/etc/rc.d/sshd_vindbrygga':
    ensure => file,
    source => 'puppet:///modules/freebsd/vindbrygga/sshd_vindbrygga',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0755',
  }

  file_line { 'vindbrygga IPv4':
    path => '/etc/rc.conf',
    line => 'ifconfig_igb0_alias0="inet 130.236.254.222 netmask 255.255.255.0"',
  }

  file_line { 'vindbrygga IPv6':
    path => '/etc/rc.conf',
    line => 'ifconfig_igb0_alias1="inet6 2001:6b0:17:f0a0::de prefixlen 64"',
  }

  file_line { 'enable vindbrygga':
    path => '/etc/rc.conf',
    line => 'sshd_vindbrygga_enable="YES"',
  }

  file_line { 'make sshd_vindbrygga use blacklistd':
    path => '/etc/rc.conf',
    line => 'sshd_vindbrygga_flags="-o UseBlacklist=yes"',
  }
}
