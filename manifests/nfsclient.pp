class freebsd::nfsclient {
  file { '/etc/auto_master':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/autofs/auto_master',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
    notify => Exec['flush_autofs_caches'],
  }

  file { '/etc/autofs/include_ldap':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/autofs/include_ldap',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0755',
    notify => Exec['flush_autofs_caches'],
  }

  file { '/etc/autofs/include':
    ensure => 'link',
    target => '/etc/autofs/include_ldap',
    notify => Exec['flush_autofs_caches'],
  }

  file { '/home':
    ensure => directory,
  }

  file { '/mp':
    ensure => directory,
  }

  file { '/mp/lysator':
    ensure => directory,
  }

  file { '/mp/jukebox':
    ensure => directory,
  }

  file { '/mp/www':
    ensure => directory,
  }

  # Delete old ceph-based mounts.
  file_line { 'Remove Ceph mounts':
    ensure            => absent,
    path              => '/etc/fstab',
    match             => '/ceph-home/',
    match_for_absence => true,
    multiple          => true,
  }

  file_line { 'Mount home':
    path => '/etc/fstab',
    line => 'home:/tank/users	/home	nfs	vers=4,minorversion=2,nosuid,rw,sec=sys,bgnow,retrycnt=3	0	0',
  }

  file_line { 'Mount mail':
    path => '/etc/fstab',
    line => 'mail:/home	/var/mail	nfs	vers=4,minorversion=2,nosuid,rw,sec=sys,bgnow,retrycnt=3	0	0',
  }

  file_line { 'Mount lysator':
    path => '/etc/fstab',
    line => 'home:/tank/lysator	/mp/lysator	nfs	vers=4,minorversion=2,nosuid,rw,sec=sys,bgnow,retrycnt=3	0	0',
  }

  file_line { 'Mount jukebox':
    path => '/etc/fstab',
    line => 'peking:/lin-pool/jukebox   /mp/jukebox     nfs     vers=4,minorversion=2,nosuid,rw,sec=sys,bgnow,retrycnt=3        0       0',
  }

  file_line { 'Mount www':
    path => '/etc/fstab',
    line => 'knuth:/srv/www	/mp/www	nfs	vers=4,minorversion=2,nosuid,rw,sec=sys,bgnow,retrycnt=3	0	0',
  }

  # Flush caches so changes take effect immediately
  exec { 'flush_autofs_caches':
    command     => '/usr/sbin/automount -c',
    refreshonly => true,
  }

  # nfsuserd should be active for NFSv4 compliance
  service { 'nfsuserd':
    ensure => running,
    enable => true,
  }
}
