#
class freebsd::workstation::inteldrm {
  file { '/etc/devfs_lysator.rules':
      ensure => file,
      source => 'puppet:///modules/freebsd/devfs_lysator.rules',
      owner  => 'root',
      group  => 'wheel',
      mode   => '0644'
  }
  file { '/etc/rc.conf.d/devfs':
      ensure => file,
      source => 'puppet:///modules/freebsd/rc.conf.d/devfs',
      owner  => 'root',
      group  => 'wheel',
      mode   => '0644'
  }
}
