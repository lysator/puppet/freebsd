#
class freebsd::blacklistd {
  file { '/etc/rc.conf.d/blacklistd':
    ensure => file,
    source => 'puppet:///modules/freebsd/rc.conf.d/blacklistd',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }
  file { '/etc/ipfw-blacklist.rc':
    ensure  => present,
    content => '',
  }
  file_line { 'make sshd use blacklistd':
    path => '/etc/rc.conf',
    line => 'sshd_flags="-o UseBlacklist=yes"',
  }
}
