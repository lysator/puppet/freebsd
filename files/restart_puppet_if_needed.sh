#!/usr/bin/env bash

# Automatically restart puppet if ruby or puppet has been updated

puppet_agent_process=$(LC_TIME=C ps -axwwo pid,lstart,command  --libxo json,pretty | jq '."process-information".process[] | select(.command | test("^/usr/local/bin/ruby[0-9]+ /usr/local/bin/puppet agent"))')

if [ -z "$puppet_agent_process" ]; then
  exit
fi

puppet_agent_start_time=$(echo "$puppet_agent_process" | jq -r '."start-time"' | head -1)
ruby_interpreter=$(echo "$puppet_agent_process" | jq -r '.command' | head -1)
ruby_interpreter=${ruby_interpreter%% *}

puppet_agent_start_time_timestamp=$(LC_TIME=C date -f "%c" -j "$puppet_agent_start_time" +%s)


ruby_interpreter_timestamp=$(stat -f '%m' "$ruby_interpreter")
puppet_executable_timestamp=$(stat -f '%m' /usr/local/bin/puppet)

# missing ruby interpreter likely means it has been replaced by a newer version (e.g. ruby32 -> ruby33)
if ! [ -e "$ruby_interpreter" ] || [ "$puppet_agent_start_time_timestamp" -le "$ruby_interpreter_timestamp" ] || [ "$puppet_agent_start_time_timestamp" -le "$puppet_executable_timestamp" ]; then
  service puppet restart
fi
