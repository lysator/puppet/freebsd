#
class freebsd::workstation::claptrap {
    file { '/usr/local/etc/X11/xorg.conf':
      ensure => file,
      source => 'puppet:///modules/freebsd/workstation/claptrap-xorg.conf',
      owner  => 'root',
      group  => 'wheel',
      mode   => '0644',
    }
}
