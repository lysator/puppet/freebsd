class freebsd::ldap {
  package {
    [
      'nss-pam-ldapd',
      'openldap26-client',
    ]:
      ensure => installed,
  }

  file { '/etc/nsswitch.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/nsswitch.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/krb5.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/krb5.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/sshd':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/pam.d/sshd',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/system':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/pam.d/system',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/su':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/pam.d/su',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/etc/nslcd.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/nslcd.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  service { 'nslcd':
    ensure => running,
    enable => true,
  }

  file { '/usr/local/etc/openldap/ldap.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/openldap/ldap.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/etc/ipa':
    ensure => directory,
  }

  file { '/usr/local/etc/ipa/ca.crt':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/ipa/ca.crt',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }
}
