class freebsd::ldap::user {
  file_line { 'Allow normal user login':
    path   => '/etc/master.passwd',
    line   => '+:::::::::',
    notify => Exec['Update /etc/passwd'],
  }
  exec { 'Update /etc/passwd':
    command     => '/usr/sbin/pwd_mkdb -p /etc/master.passwd',
    refreshonly => true,
  }
}
