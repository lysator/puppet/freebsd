# denna klass ser till så att konfigurationsfiler till cups är där de ska.
class freebsd::workstation::printer {
  package {
    [
      'cups',
    ]:
      ensure => installed,
  }
  -> service { 'cupsd':
    enable => true,
    ensure => running,
  }
  -> file { '/usr/local/etc/cups/ppd/urd.ppd':
    source => 'puppet:///modules/freebsd/workstation/printing/xrx6510.ppd',
    owner  => 'root',
    group  => 'cups',
    mode   => '0640',
  }
  -> file { '/usr/local/etc/cups/printers.conf':
    source => 'puppet:///modules/freebsd/workstation/printing/printers.conf',
    owner  => 'root',
    group  => 'cups',
    mode   => '0600',
  }
  ~> exec { '/usr/sbin/service cupsd restart':
    refreshonly => true,
  }
}
