#
class freebsd::workstation::slim {
  file { '/usr/local/etc/slim.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/workstation/slim.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/share/slim-lysator/themes/':
    ensure  => directory,
    recurse => true,
    source  => 'puppet:///modules/freebsd/workstation/slim.themes/',
    owner   => 'root',
    group   => 'wheel',
    mode    => '0644',
  }

  service { 'slim':
    ensure => running,
    enable => true,
  }

  file { '/usr/local/share/xsessions/xmonad.desktop':
    ensure => file,
    source => 'puppet:///modules/freebsd/workstation/xmonad.desktop',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }
}
